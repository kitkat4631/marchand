// Créez une classe Produit :

// nom
// prix

// Créez une classe Panier avec :

// une méthode ajoute( produit )
// une méthode retire( produit )
// une proprieté totalHT
// une proprieté totalTTC

// Utilisation : ajouter ce qu'il faut à ce code de base pour qu'il fonctionne.
//     var baguette = new Produit( 'Baguette', 0.85); // prix HT
//     var croissant = new Produit( 'Croissant', 0.80);

//     var panier = new Panier();
//     panier.ajoute(baguette);
//     panier.ajoute(croissant);

//     console.log(panier.totalHT);
//     console.log(panier.totalTTC);   
// Bonus : reprendre cet exemple en ajoutant une classe Viennoiserie qui hérite de la classe Produit en lui ajoutant l'attribut booléen frais.

function Produit(n, p)
{
	this.nom = n;
	this.prix = p;
}
function Panier(totalHT, totalTTC)
{
	this.produits = [];
	this.totalHT = 0;
	this.totalTTC = 0;
	this.TVA = 1.055;

	this.ajouter = function(p)
	{
		this.produits.push(p);
		this.totalHT = this.totalHT + p.prix;
		this.totalTTC = this.totalHT * this.TVA;
	}
}
   
 var baguette = new Produit( 'Baguette', 0.85); 
 var croissant = new Produit( 'Croissant', 0.80);
 var panier = new Panier();

     panier.ajouter(baguette);
     panier.ajouter(croissant);

     console.log(baguette);
     console.log(croissant);
     console.log(panier);
     console.log(panier.totalHT);
     console.log(panier.totalTTC);
